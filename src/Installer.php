<?php

namespace Asenso\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class Installer extends LibraryInstaller
{
    /**
     * @var array
     */
    public $packages = [
        "asenso-module",
        "asenso-theme"
    ];
    
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $type = $package->getType();
        $names = $package->getNames();
        
        if (is_array($names)) {
            $names = $names[0];
        }
        
        if ("asenso-module" == $type) {
            list($vendor, $package) = explode("/", $names);
            
            return "modules/".$vendor."/".$package;
        }
        
        if ("asenso-theme" == $type) {
            list($vendor, $package) = explode("/", $names);
            
            return "themes/".$vendor."/".$package;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return in_array($packageType, $this->packages);
    }
}