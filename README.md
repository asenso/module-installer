# Asenso Module Installer

## Usage

Plugin packages are automatically loaded as soon as they are installed and will be loaded
when composer starts up if they are found in the current project's list of installed packages.
Additionally all plugin packages installed in the `COMPOSER_HOME` directory using the composer
global command are loaded before local project plugins are loaded.

>You may pass the `--no-plugins` options to composer commands to disable all installed plugins.
>This may be particularly helpful if any of the plugins causes errors and you wish to update or
>uninstall it.

<em>https://getcomposer.org/doc/articles/plugins.md#using-plugins</em>